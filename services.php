<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>VR Holidays Travel Agency, - Hotel Online Booking</title>
        <meta name="keywords" content=""/>
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/bootstrap/bootstrap.css" rel="stylesheet" media="screen" async>
        <!-- <link href="css/bootstrap/bootstrap-theme.css" rel="stylesheet" media="screen" async> -->
        <!-- <link href="css/bootstrap/bootstrap-slider.css" rel="stylesheet" media="screen" async> -->
        <link href="css/nav/style.css" rel="stylesheet" media="screen" async>
        <!-- <link href="js/fancybox/jquery.fancybox.css" rel="stylesheet" media="screen" async> -->
        <link href="css/skins/theme-options.css" rel="stylesheet" media="screen" async>
        <link href="css/carousel/owl.carousel.css" rel="stylesheet" media="screen" async>
        <link href="css/carousel/owl.theme.css" rel="stylesheet" media="screen" async>
        <link href="css/icons/font-awesome.css" rel="stylesheet" media="screen" async>
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,400,300,700" rel="stylesheet" media="screen" async>
        <link href="http://fonts.googleapis.com/css?family=Raleway" rel="stylesheet" media="screen" async>
        <link href="css/style.css" rel="stylesheet" media="screen" async>
        <link href="css/skins/green/green.css" rel="stylesheet" media="screen" async>
        <link href="css/theme-responsive.css" rel="stylesheet" media="screen" async>
        <link rel="shortcut icon" href="img/favicon-icon.png">
        <!-- <link rel="apple-touch-icon" href="img/icons/apple-touch-icon.png">
        <link rel="apple-touch-icon" sizes="72x72" href="img/icons/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="114x114" href="img/icons/apple-touch-icon-114x114.png"> -->
        <script src="js/modernizr.js"></script>
        <!--[if IE]>
                    <link rel="stylesheet" href="css/ie/ie.css">
                <![endif]-->
        <!--[if lte IE 8]>
                    <script src="js/responsive/html5shiv.js"></script>
                    <script src="js/responsive/respond.js"></script>
                <![endif]-->
    </head>
    <body>
        <div id="layout">
            <?php include_once './header.php'; ?>
            <div class="section-title-01">
                <div class="bg_parallax image_03_parallax"></div>
                <div class="opacy_bg_02">
                    <div class="container">
                        <h1>Services</h1>
                        <div class="crumbs">
                            <ul>
                                <li><a href="index.php">Home</a></li>
                                <li>/</li>
                                <li>Services</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <section class="content-central">
                <div class="semiboxshadow text-center">
                    <img src="img/img-theme/shp.png" class="img-responsive" alt="">
                </div>
                <div class="content_info">
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="services-lines full-services">
                                <li>
                                    <div class="item-service-line">
                                        <i class="fa fa-plane"></i>
                                        <h5>Travel Arrangements</h5>
                                    </div>
                                </li>
                                <li>
                                    <div class="item-service-line">
                                        <i class="fa fa-hotel"></i>
                                        <h5>Handpicked Hotels</h5>
                                    </div>
                                </li>
                                <li>
                                    <div class="item-service-line">
                                        <i class="fa fa-ticket"></i>
                                        <h5>Ticketing</h5>
                                    </div>
                                </li>
                                <li>
                                    <div class="item-service-line">
                                        <i class="fa fa-address-card"></i>
                                        <h5>Visa</h5>
                                    </div>
                                </li>
                                <li>
                                    <div class="item-service-line">
                                        <i class="fa fa-ship"></i>
                                        <h5>Cruise</h5>
                                    </div>
                                </li>
                                <li>
                                    <div class="item-service-line">
                                        <i class="fa fa-suitcase"></i>
                                        <h5>Tour Packages</h5>
                                    </div>
                                </li>
                                <li>
                                    <div class="item-service-line">
                                        <i class="fa fa-bank"></i>
                                        <h5>Best Price Guarantee</h5>
                                    </div>
                                </li>
                                <li>
                                    <div class="item-service-line">
                                        <i class="fa fa-shield"></i>
                                        <h5>Trust & Safety</h5>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="content_info">
                    <div class="skin_base color-white">
                        <div class="container">
                            <div class="row">
                                <div class="titles">
                                <h2><span>¿</span>Why <span>Book</span> in <span>VR Holidays</span><span>?</span></h2>
                                    <i class="fa fa-plane"></i>
                                    <hr class="tall">
                                </div>
                            </div>
                        </div>
                        <div class="container">
                            <div class="row padding-bottom">
                                <div class="col-md-4 text-center">
                                    <div class="item-feature text-center">
                                        <div class="head-feature">
                                            <span></span>
                                            <br>
                                            <div class="title-feature">
                                                <i class="fa fa-sun-o left-icon"></i>
                                                <i class="fa fa-umbrella right-icon"></i>
                                                Great
                                            </div>
                                            <br>
                                            <span>discount</span>
                                        </div>
                                        <div class="info-feature">
                                            <p>For over years offering the best plans at the best price with exclusive Services & discounts.</p>
                                        </div>
                                    </div>
                                </div>
                            <div class="col-md-4 text-center">
                                <div class="item-feature text-center">
                                    <div class="head-feature">
                                        <span>Better</span>
                                        <br>
                                        <div class="title-feature">
                                            <i class="fa fa-cog left-icon"></i>
                                            <i class="fa fa-globe right-icon"></i>
                                            Service 
                                        </div>
                                        <br>
                                        <span>Experience</span>
                                    </div>
                                    <div class="info-feature">
                                        <p>Experience Great service by VR holidays
                                         while you are on vacation and make best memorable Vacation with your family and friends.</p>
                                    </div>
                                </div>
                            </div>
                                <div class="col-md-4 text-center">
                                    <div class="item-feature text-center">
                                        <div class="head-feature">
                                            <span>Pay </span>
                                            <br>
                                            <div class="title-feature">
                                                <i class="fa fa-thumbs-o-up left-icon"></i>
                                                <i class="fa fa-usd right-icon dollar-width"></i>
                                                The Best 
                                            </div>
                                            <br>
                                            <span>Price</span>
                                            </div>
                                        <div class="info-feature">
                                            <p>All days Escape at the best price in offers for our products.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--<div class="content_info">
                    <div class="bg_parallax image_04_parallax"></div>
                    <div class="opacy_bg_02 padding-bottom">
                        <div class="container">
                            <div class="row">
                                <div class="titles">
                                    <h2>The <span>Promotions</span> Bonus</h2>
                                    <i class="fa fa-money"></i>
                                    <hr class="tall">
                                </div>
                            </div>
                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-6 col-md-3">
                                    <div class="promotion-box">
                                        <div class="promotion-box-header">
                                            <img src="img/sponsors/1.png" alt="img">
                                        </div>
                                        <div class="promotion-box-center color-1">
                                            <div class="prince">
                                                30
                                            </div>
                                            <div class="percentage">
                                                %<span>discount</span>.
                                            </div>
                                        </div>
                                        <div class="promotion-box-info">
                                            <p>Select a bonus 30% and access to all our discounts.</p>
                                            <a href="#" class="btn btn-primary">View Details</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="promotion-box">
                                        <div class="promotion-box-header">
                                            <img src="img/sponsors/2.png" alt="img">
                                        </div>
                                        <div class="promotion-box-center color-2">
                                            <div class="prince">
                                                70
                                            </div>
                                            <div class="percentage">
                                                %<span>discount</span>.
                                            </div>
                                        </div>
                                        <div class="promotion-box-info">
                                            <p>Select a bonus 70% and access to all our discounts.</p>
                                            <a href="#" class="btn btn-primary">View Details</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="promotion-box">
                                        <div class="promotion-box-header">
                                            <img src="img/sponsors/5.png" alt="img">
                                        </div>
                                        <div class="promotion-box-center color-3">
                                            <div class="prince">
                                                50
                                            </div>
                                            <div class="percentage">
                                                %<span>discount</span>.
                                            </div>
                                        </div>
                                        <div class="promotion-box-info">
                                            <p>Select a bonus 50% and access to all our discounts.</p>
                                            <a href="#" class="btn btn-primary">View Details</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-3">
                                    <div class="promotion-box">
                                        <div class="promotion-box-header">
                                            <img src="img/sponsors/3.png" alt="img">
                                        </div>
                                        <div class="promotion-box-center color-4">
                                            <div class="prince">
                                                60
                                            </div>
                                            <div class="percentage">
                                                %<span>discount</span>.
                                            </div>
                                        </div>
                                        <div class="promotion-box-info">
                                            <p>Select a bonus 60% and access to all our discounts.</p>
                                            <a href="#" class="btn btn-primary">View Details</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content_info">
                    <div class="container">
                        <div class="row">
                            <div class="titles">
                                <h2>Our <span>Sponsors</span></h2>
                                <i class="fa fa-plane"></i>
                                <hr class="tall">
                            </div>
                        </div>
                    </div>
                    <div class="container">
                        <div class="row padding-bottom">
                            <div class="col-md-12">
                                <ul id="sponsors" class="tooltip-hover">
                                    <li data-toggle="tooltip" title data-original-title="Name Sponsor"> <a href="#"><img src="img/sponsors/1.png" alt="Image"></a></li>
                                    <li data-toggle="tooltip" title data-original-title="Name Sponsor"> <a href="#"><img src="img/sponsors/2.png" alt="Image"></a></li>
                                    <li data-toggle="tooltip" title data-original-title="Name Sponsor"> <a href="#"><img src="img/sponsors/3.png" alt="Image"></a></li>
                                    <li data-toggle="tooltip" title data-original-title="Name Sponsor"> <a href="#"><img src="img/sponsors/4.png" alt="Image"></a></li>
                                    <li data-toggle="tooltip" title data-original-title="Name Sponsor"> <a href="#"><img src="img/sponsors/5.png" alt="Image"></a></li>
                                    <li data-toggle="tooltip" title data-original-title="Name Sponsor"> <a href="#"><img src="img/sponsors/6.png" alt="Image"></a></li>
                                    <li data-toggle="tooltip" title data-original-title="Name Sponsor"> <a href="#"><img src="img/sponsors/7.png" alt="Image"></a></li>
                                    <li data-toggle="tooltip" title data-original-title="Name Sponsor"> <a href="#"><img src="img/sponsors/8.png" alt="Image"></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>-->
                <div class="content_info">
                    <div class="bg_parallax image_02_parallax"></div>
                    <div class="opacy_bg_02">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="info-testimonial info-testimonial-grey">
                                        <ul id="testimonials">
                                            <li>
                                                <p><i class="fa fa-quote-left"></i>The hotel is stunning, super recommended. And the treatment of its people in very good, are always attentive to the little things.<i class="fa fa-quote-right"></i></p>
                                                <div class="image-testimonials">
                                                    <img src="img/testimonials/1.jpg" alt="">
                                                </div>
                                                <h4>Jeniffer Martinez</h4>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </li>
                                            <li>
                                                <p><i class="fa fa-quote-left"></i>Spectacular, excellent personal, restaurants and very good drinks, highly recommended.<i class="fa fa-quote-right"></i></p>
                                                <div class="image-testimonials">
                                                    <img src="img/testimonials/2.jpg" alt="">
                                                </div>
                                                <h4>Juan Rendon</h4>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star-half-o"></i>
                                            </li>
                                            <li>
                                                <p><i class="fa fa-quote-left"></i>The hotel is stunning, super recommended. And the treatment of its people in very good, are always attentive to the little things.<i class="fa fa-quote-right"></i></p>
                                                <div class="image-testimonials">
                                                    <img src="img/testimonials/3.jpg" alt="">
                                                </div>
                                                <h4>Federick Gordon</h4>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star-half-o"></i>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="testimonial-info">
                                        <h2>TESTIMONIALS</h2>
                                        <P>You can choose your favorite destination and start planning your long-awaited vacation. We offer thousands of destinations and have a wide variety of hotels so that you can host and enjoy your stay without problems. Book now your trip travelia.com.</P>
                                        <a href="#" class="btn btn-primary">View Details</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <?php include_once './footer.php'; ?>
        </div>
        
        <script src="js/jquery.js"></script>
        <script src="js/jquery-ui.1.10.4.min.js"></script>
        <script type="text/javascript" src="js/theme-options/jquery.cookies.js"></script>
        <script type="text/javascript" src="js/bootstrap/bootstrap.js"></script>
        <script src="js/nav/jquery.sticky.js" type="text/javascript"></script>
        <script src="js/carousel/carousel.js"></script>
        <!-- <script type="text/javascript" src="js/totop/jquery.ui.totop.js"></script> -->
        <!-- <script type="text/javascript" src="js/rs-plugin/js/jquery.themepunch.tools.min.js"></script> -->
        <!-- <script type='text/javascript' src='js/rs-plugin/js/jquery.themepunch.revolution.min.js'></script> -->
        <!-- <script type="text/javascript" src="js/fancybox/jquery.fancybox.js"></script> -->
        <!-- <script src="js/filters/jquery.isotope.js" type="text/javascript"></script> -->
        <!-- <script type="text/javascript" src="js/bootstrap/bootstrap-slider.js"></script> -->
        <!-- <script type="text/javascript" src="js/main.js"></script> -->
        <script type="text/javascript">
        $(document).ready(function() {
            
                 $("#testimonials").owlCarousel({
                     items: 1,
                     autoPlay: 3200,
                     navigation: false,
                     autoHeight: true,
                     slideSpeed: 400,
                     singleItem: true,
                     pagination: true
                 });
        }); 
    </script>
    </body>
</html>