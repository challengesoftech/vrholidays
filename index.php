<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>VR Holidays Travel Agency, Hotel Online Booking</title>
        <meta name="keywords" content="Travel Agency, Hotel Online Booking"/>
        <meta name="description" content="Book Flight, Hotels and International / Domestic Holiday Packages with Great Deals">
        <meta name="author" content="VR Holidays">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/bootstrap/bootstrap.css" rel="stylesheet" media="screen" async>
        <!-- <link href="css/bootstrap/bootstrap-theme.css" rel="stylesheet" media="screen" async> -->
        <!-- <link href="css/bootstrap/bootstrap-slider.css" rel="stylesheet" media="screen" async> -->
        <link href="css/nav/style.css" rel="stylesheet" media="screen" async>
        <!-- <link href="js/fancybox/jquery.fancybox.css" rel="stylesheet" media="screen" async> -->
        <link href="css/skins/theme-options.css" rel="stylesheet" media="screen" async>
        <link href="css/carousel/owl.carousel.css" rel="stylesheet" media="screen" async>
        <link href="css/carousel/owl.theme.css" rel="stylesheet" media="screen" async>
        <link href="css/icons/font-awesome.css" rel="stylesheet" media="screen" async>
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,400,300,700" rel="stylesheet" media="screen" async>
        <link href="http://fonts.googleapis.com/css?family=Raleway" rel="stylesheet" media="screen" async>
        <link href="css/style.css" rel="stylesheet" media="screen" async>
        <link href="css/skins/green/green.css" rel="stylesheet" media="screen" async>
        <link href="css/theme-responsive.css" rel="stylesheet" media="screen" async>
        <link rel="shortcut icon" href="img/favicon-icon.png">
        <!-- <link rel="apple-touch-icon" href="img/icons/apple-touch-icon.png">
        <link rel="apple-touch-icon" sizes="72x72" href="img/icons/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="114x114" href="img/icons/apple-touch-icon-114x114.png"> -->
        <script src="js/modernizr.js"></script>
        <!--[if IE]>
                    <link rel="stylesheet" href="css/ie/ie.css">
                <![endif]-->
        <!--[if lte IE 8]>
                    <script src="js/responsive/html5shiv.js"></script>
                    <script src="js/responsive/respond.js"></script>
                <![endif]-->
    </head>
    <body>
        <div id="layout">
            <?php include_once './header.php'; ?>
            <section class="tp-banner-container">
                <div class="tp-banner">
                    <ul>
                        <li data-transition="slidevertical" data-slotamount="1" data-masterspeed="1000" data-saveperformance="off" data-title="Slide">
                            <img src="img/slide/1.jpg" alt="fullslide1" data-bgposition="center center" data-kenburns="on" data-duration="6000" data-ease="Linear.easeNone" data-bgfit="130" data-bgfitend="100" data-bgpositionend="right center">
                        </li>
                        <li data-transition="slidehorizontal" data-slotamount="1" data-masterspeed="1000" data-saveperformance="off" data-title="Slide">
                            <img src="img/slide/2.jpg" alt="fullslide2" data-bgposition="top center" data-kenburns="on" data-duration="6000" data-ease="Linear.easeNone" data-bgfit="130" data-bgfitend="100" data-bgpositionend="right center">
                        </li>
                        <li data-transition="slidevertical" data-slotamount="1" data-masterspeed="1000" data-saveperformance="off" data-title="Slide">
                            <img src="img/slide/3.jpg" alt="fullslide3" data-bgposition="right center" data-kenburns="on" data-duration="6000" data-ease="Linear.easeNone" data-bgfit="130" data-bgfitend="100" data-bgpositionend="right center">
                        </li>
                        <li data-transition="slidevertical" data-slotamount="1" data-masterspeed="1000" data-saveperformance="off" data-title="Slide">
                            <img src="img/slide/4.jpg" alt="fullslide4" data-bgposition="left center" data-kenburns="on" data-duration="6000" data-ease="Linear.easeNone" data-bgfit="130" data-bgfitend="100" data-bgpositionend="right center">
                        </li>
                    </ul>
                    <div class="tp-bannertimer"></div>
                </div>
                <!--                <div class="filter-title">
                                    <div class="title-header">
                                        <h1>BOOK A WHOLE HOME</h1>
                                        <p class="lead">Book cheap hotels and make payment facilities, free cancellation when the hotel so provides, compare prices and find all the options for your vacation.</p>
                                    </div>
                                    <div class="filter-header">
                                        <form action="#">
                                            <input type="text" required="required" placeholder="Where do you want to go?" class="input-large">
                                            <input type="text" required="required" placeholder="Check In" class="date-input">
                                            <input type="text" required="required" placeholder="Check Out" class="date-input">
                                            <div class="selector">
                                                <select class="guests-input">
                                                    <option value="1">1 Guests</option>
                                                    <option value="2">2 Guests</option>
                                                    <option value="3">3 Guests</option>
                                                    <option value="4">4 Guests</option>
                                                    <option value="5">5+ Guests</option>
                                                </select>
                                                <span class="custom-select">Guests</span>
                                            </div>
                                            <input type="submit" value="Search">
                                        </form>
                                    </div>
                                </div>-->
            </section>
            <section class="content-central">
                <div class="semiboxshadow text-center">
                    <img src="img/img-theme/shp.png" class="img-responsive" alt="">
                </div>
                <div class="content_info">
                    <div class="content_resalt">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-4 ">
                                    <div class="services-lines-info">
                                        <h3>WELCOME TO VR HOLIDAYS</h3>
                                        <p class="lead">
                                        Book Flight, Hotels and International / Domestic Holiday Packages with Great Deals
                                            <span class="line"></span>
                                        </p>
                                        <p>Find a wide variety of Airline tickets and Cheap flights, hotels, tour packages, car rentals, cruises and more in <a href="/">vrholidays.asia</a> .You can choose your favorite destination and start planning your long-awaited vacation. You can also check availability of flights.
</p>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <ul class="services-lines">
                                        <li>
                                            <div class="item-service-line">
                                            <i class="fa fa-plane"></i>
                                                <h5>Travel Arrangements</h5>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="item-service-line">
                                            <i class="fa fa-hotel"></i>
                                                <h5>Handpicked Hotels</h5>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="item-service-line">
                                                <i class="fa fa-ticket"></i>
                                                <h5>Ticketing</h5>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="item-service-line">
                                                <i class="fa fa-address-card"></i>
                                                <h5>Visa</h5>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="item-service-line">
                                                <i class="fa fa-ship"></i>
                                                <h5>Cruise</h5>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="item-service-line">
                                                <i class="fa fa-suitcase"></i>
                                                <h5>Tour Packages</h5>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="item-service-line">
                                                <i class="fa fa-bank"></i>
                                                <h5>Best Price Guarantee</h5>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="item-service-line">
                                                <i class="fa fa-shield"></i>
                                                <h5>Trust & Safety</h5>
                                            </div>
                                        </li>
                                    </ul>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="content_info">
                    <div class="container">
                        <div class="row">
                            <div class="titles">
                                <h2>The <span>Popular</span> Destinations </h2>
                                <i class="fa fa-map-marker "></i>
                                <hr class="tall">
                            </div>
                        </div>
                    </div>
                    <div class="content-boxes">
                        <div class="item-boxed">
                            <div class="image-boxed">
                                <span class="overlay"></span>
                                <img src="img/gallery-1/1.jpg"alt="Popular1">
                                <a href="#" class="more-boxe"><i class="fa fa-plus-circle"></i></a>
                            </div>
                            <div class="info-boxed boxed-top">
                                <h3>
                                    INDIA
                                </h3>
                                <hr class="separator">
                                <p>India is the right place for wild safaris, desert safaris, nature seeking places, 
                                    honeymoon spot, adventurous activities, spiritual places, and historical places of India.</p>
                               <!-- <ul class="starts">
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star-half-empty"></i></a></li>
                                </ul>
                                <div class="content-btn"><a href="hotel-detailed.html" class="btn btn-primary">View Details</a></div>
                                 <div class="price"><span>$</span> 45</div> -->
                            </div>
                        </div>
                        <div class="item-boxed">
                            <div class="image-boxed">
                                <span class="overlay"></span>
                                <img src="img/gallery-1/2.jpg" alt="Popular2">
                                <a href="#" class="more-boxe"><i class="fa fa-plus-circle"></i></a>
                            </div>
                            <div class="info-boxed boxed-top">
                                <h3>
                                EUROPE<br>
                                </h3>
                                <hr class="separator">
                                <p>European countries are full of vibrant cities known for their beaches, museums, restaurants, nightlife and architecture. 
                                So, it comes as no surprise that deciding which ones to visit can be difficult.</p>
                              <!--  <ul class="starts">
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star-half-empty"></i></a></li>
                                </ul>
                                <div class="content-btn"><a href="hotel-detailed.html" class="btn btn-primary">View Details</a></div>
                                 <div class="price"><span>$</span> 65</div> -->
                            </div>
                        </div>
                        <div class="item-boxed">
                            <div class="image-boxed image-bottom">
                                <span class="overlay"></span>
                                <img src="img/gallery-1/3.jpg" alt="Popular3">
                                <a href="#" class="more-boxe"><i class="fa fa-plus-circle"></i></a>
                            </div>
                            <div class="info-boxed boxed-bottom">
                                <h3>
                                MALDIVES<br>
                                </h3>
                                <hr class="separator">
                                <p>The Maldives, a tropical haven of immaculate beaches located in the Indian Ocean south of Sri Lanka,
                                 is an archipelago of 1,192 coral islands grouped into natural atolls, out of which only a few are inhabited. 
                                </p>
                                <!--<ul class="starts">
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star-half-empty"></i></a></li>
                                </ul>
                                <div class="content-btn"><a href="hotel-detailed.html" class="btn btn-primary">View Details</a></div>
                                 <div class="price"><span>$</span> 30</div> -->
                            </div>
                        </div>
                        <div class="item-boxed">
                        <div class="image-boxed image-bottom">
                                <span class="overlay"></span>
                                <img src="img/gallery-1/4.jpg" alt="Popular4">
                                <a href="#" class="more-boxe"><i class="fa fa-plus-circle"></i></a>
                        </div>
                            <div class="info-boxed boxed-bottom">
                                <h3>
                                    DUBAI<br>            
                                </h3>
                                <hr class="separator">
                                <p>Indulgent, glamorous, and progressive are words that describe Dubai. 
                                This United Arab Emirates city is a luxury travel destination for leisure and business travelers.
                                </p>
                                <!--<ul class="starts">
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star"></i></a></li>
                                    <li><a href="#"><i class="fa fa-star-half-empty"></i></a></li>
                                </ul>
                                <div class="content-btn"><a href="hotel-detailed.html" class="btn btn-primary">View Details</a></div>
                                <div class="price"><span>$</span> 80</div> -->
                            </div>
                            
                        </div>
                    </div>
                </div>
                <div class="content_info">
                    <div class="container">
                        <div class="row">
                            <div class="titles">
                                <h2><span>¿</span>Why <span>Book</span> in <span>VR Holidays</span><span>?</span></h2>
                                <i class="fa fa-plane"></i>
                                <hr class="tall">
                            </div>
                        </div>
                    </div>
                    <div class="container">
                        <div class="row padding-bottom">
                            <div class="col-md-4">
                                <div class="item-feature text-center">
                                    <div class="head-feature">
                                        <!-- <span>Up</span> -->
                                        <br>
                                        <div class="title-feature">
                                            <i class="fa fa-sun-o left-icon"></i>
                                            <i class="fa fa-umbrella right-icon"></i>
                                            Great
                                        </div>
                                        <!-- <span>%</span> -->
                                        <br>
                                        <span>discount</span>
                                    </div>
                                    <div class="info-feature">
                                        <p>For over years offering the best plans at the best price with exclusive Services & discounts.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 text-center">
                                <div class="item-feature text-center">
                                    <div class="head-feature">
                                        <span>Better</span>
                                        <br>
                                        <div class="title-feature">
                                            <i class="fa fa-cog left-icon"></i>
                                            <i class="fa fa-globe right-icon"></i>
                                            Service 
                                        </div>
                                        <br>
                                        <span>Experience</span>
                                    </div>
                                    <div class="info-feature">
                                        <p>Experience Great service by VR holidays
                                         while you are on vacation and make best memorable Vacation with your family and friends.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 text-center">
                                <div class="item-feature text-center">
                                    <div class="head-feature">
                                        <span>Pay</span>
                                        <br>
                                        <div class="title-feature">
                                            <i class="fa fa-thumbs-o-up left-icon"></i>
                                            <i class="fa fa-usd right-icon dollar-width"></i>
                                            The Best 
                                        </div>
                                        <br>
                                            <span>Price</span>
                                        <!-- <span>+ low</span> -->
                                    </div>
                                    <div class="info-feature">
                                        <p>All days Escape at the best price in offers for our products.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content_info content_resalt">
                    <ul id="slide-features">
                        <li>

                            <div class="container">
                                <div class="row">

                                    <div class="col-md-5">
                                        <div class="services-lines-info">
                                            <h3>We Provide You An Ultimate Travel Experience</h3>
                                            <p class="lead">
                                                Book cheap travel packages and make payment facilities.
                                                <span class="line"></span>
                                            </p>
                                            <p>Find a wide variety of airline tickets and cheap flights, hotels, tour packages, car rentals, cruises and more in vrholidays.aisa .You can choose your favorite destination and start planning your long-awaited vacation. You can also check availability of flights and hotels quickly and easily, in order to find the option that best suits your needs.</p>
                                            <br class="visible-lg">
                                            <p>Book cheap hotels and make payment facilities, free cancellation when the hotel so provides, compare prices and find all the options for your vacation.</p>
                                            <br>
                                            <!-- <a href="#" class="btn btn-primary">View Details</a> -->
                                        </div>
                                    </div>
                                    <div class="col-md-7">
                                        <img src="img/service.png" class="img-responsive" alt="travel">
                                    </div>
                                </div>
                            </div>

                        </li>

<!--                        <li>
                            <div class="container">
                                <div class="row padding-top">
                                    <div class="col-md-6">
                                        <div class="img-hover">
                                            <div class="overlay"> <a href="img/gallery-2/2.jpg" class="fancybox"><i class="fa fa-plus-circle"></i></a></div>
                                            <img src="img/gallery-2/2.jpg" alt="" class="img-responsive">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="services-lines-info">
                                            <h2>Explore our latest tours </h2>
                                            <p class="lead">
                                                Select Travelia Theme And Receive our discounts by e-mail.
                                                <span class="line"></span>
                                            </p>
                                            <p>You can choose your favorite destination and start planning your long-awaited vacation. We offer thousands of destinations and have a wide variety of hotels so that you can host and enjoy your stay without problems. Book now your trip travelia.com.</p>
                                            <br>
                                            <a href="#" class="btn btn-primary">View Details</a>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </li>-->

                    </ul>
                </div>


                <div class="content_info">

                    <div class="bg_parallax image_02_parallax"></div>

                    <div class="opacy_bg_02">

                        <div class="container">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="info-testimonial info-testimonial-blue">
                                        <ul id="testimonials">
                                            <li>
                                                <p><i class="fa fa-quote-left"></i>The hotel is stunning, super recommended. And the treatment of its people in very good, are always attentive to the little things.<i class="fa fa-quote-right"></i></p>
                                                <div class="image-testimonials">
                                                    <img src="img/testimonials/1.jpg" alt="testimonial1">
                                                </div>
                                                <h4>Jeniffer Martinez</h4>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </li>
                                            <li>
                                                <p><i class="fa fa-quote-left"></i>Spectacular, excellent personal, restaurants and very good drinks, highly recommended.<i class="fa fa-quote-right"></i></p>
                                                <div class="image-testimonials">
                                                    <img src="img/testimonials/2.jpg" alt="testimonial2">
                                                </div>
                                                <h4>Juan Rendon</h4>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star-half-o"></i>
                                            </li>
                                            <li>
                                                <p><i class="fa fa-quote-left"></i>The hotel is stunning, super recommended. And the treatment of its people in very good, are always attentive to the little things.<i class="fa fa-quote-right"></i></p>
                                                <div class="image-testimonials">
                                                    <img src="img/testimonials/3.jpg" alt="testimonial3">
                                                </div>
                                                <h4>Federick Gordon</h4>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star-half-o"></i>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="testimonial-info">
                                        <h2>TESTIMONIALS</h2>
                                        <P>You can choose your favorite destination and start planning your long-awaited vacation. 
                                        We offer thousands of destinations and have a wide variety of hotels so that you can host and enjoy your stay without problems. 
                                        Book now your trip with VR Holidays.</P>
                                        <a href="#" class="btn btn-primary">Share Reviews</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>


<!--                <div class="section-twitter">
                    <i class="fa fa-twitter icon-big"></i>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                                                <div class="text-center">
                                                                    <h2>Our recents <span class="text-resalt">twitts</span>.</h2>
                                                                </div>
                                                                <div id="twitter"></div>
                            </div>
                        </div>
                    </div>
                </div>-->
            </section>
            <?php include_once './footer.php'; ?>
        </div>
        <script src="js/jquery.js"></script>
        <script src="js/jquery-ui.1.10.4.min.js"></script>
        <script type="text/javascript" src="js/theme-options/jquery.cookies.js"></script>
        <script type="text/javascript" src="js/bootstrap/bootstrap.js"></script>
        <script src="js/nav/jquery.sticky.js" type="text/javascript"></script>
        <script type="text/javascript" src="js/totop/jquery.ui.totop.js"></script>
        <script type="text/javascript" src="js/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
        <script type='text/javascript' src='js/rs-plugin/js/jquery.themepunch.revolution.min.js'></script>
        <script type="text/javascript" src="js/fancybox/jquery.fancybox.js"></script>
        <script src="js/carousel/carousel.js"></script>
        <!-- <script src="js/filters/jquery.isotope.js" type="text/javascript"></script> -->
        <script type="text/javascript" src="js/bootstrap/bootstrap-slider.js"></script>
        <!-- <script type="text/javascript" src="js/main.js"></script> -->
        <script type="text/javascript">
            jQuery(document).ready(function () {
                jQuery('.tp-banner').show().revolution({
                    dottedOverlay: "none",
                    delay: 5000,
                    startwidth: 1170,
                    startheight: 970,
                    minHeight: 350,
                    navigationType: "none",
                    navigationArrows: "solo",
                    navigationStyle: "preview1"
                });
                $("#testimonials").owlCarousel({
                     items: 1,
                     autoPlay: 3200,
                     navigation: false,
                     autoHeight: true,
                     slideSpeed: 400,
                     singleItem: true,
                     pagination: true
                 });
                 $("#slide-features").owlCarousel({
                    autoPlay: false,
                    items: 1,
                    navigation: true,
                    autoHeight: true,
                    slideSpeed: 400,
                    singleItem: true,
                    pagination: true
                });
            }); //ready
        </script>

    </body>
</html>
