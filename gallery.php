<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>VR Holidays Travel Agency, - Hotel Online Booking</title>
        <meta name="keywords" content=""/>
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/bootstrap/bootstrap.css" rel="stylesheet" media="screen" async>
        <!-- <link href="css/bootstrap/bootstrap-theme.css" rel="stylesheet" media="screen" async> -->
        <!-- <link href="css/bootstrap/bootstrap-slider.css" rel="stylesheet" media="screen" async> -->
        <link href="css/nav/style.css" rel="stylesheet" media="screen" async>
        <!-- <link href="js/fancybox/jquery.fancybox.css" rel="stylesheet" media="screen" async> -->
        <link href="css/skins/theme-options.css" rel="stylesheet" media="screen" async>
        <!-- <link href="css/carousel/owl.carousel.css" rel="stylesheet" media="screen" async> -->
        <!-- <link href="css/carousel/owl.theme.css" rel="stylesheet" media="screen" async> -->
        <!-- <link href="css/icons/font-awesome.css" rel="stylesheet" media="screen" async> -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,400,300,700" rel="stylesheet" media="screen" async>
        <link href="http://fonts.googleapis.com/css?family=Raleway" rel="stylesheet" media="screen" async>
        <link href="css/style.css" rel="stylesheet" media="screen" async>
        <link href="css/skins/green/green.css" rel="stylesheet" media="screen" async>
        <link href="css/theme-responsive.css" rel="stylesheet" media="screen" async>
        <link rel="shortcut icon" href="img/favicon-icon.png">
        <!-- <link rel="apple-touch-icon" href="img/icons/apple-touch-icon.png">
        <link rel="apple-touch-icon" sizes="72x72" href="img/icons/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="114x114" href="img/icons/apple-touch-icon-114x114.png"> -->
        <script src="js/modernizr.js"></script>
        <!--[if IE]>
                    <link rel="stylesheet" href="css/ie/ie.css">
                <![endif]-->
        <!--[if lte IE 8]>
                    <script src="js/responsive/html5shiv.js"></script>
                    <script src="js/responsive/respond.js"></script>
                <![endif]-->
    </head>
    <body>
        <div id="layout">
            <?php include_once './header.php'; ?>
            <div class="section-title-01">
                <div class="bg_parallax image_04_parallax"></div>
                <div class="opacy_bg_02">
                    <div class="container">
                        <h1>Gallery</h1>
                        <div class="crumbs">
                            <ul>
                                <li><a href="index.php">Home</a></li>
                                <li>/</li>
                                <li>Gallery</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <section class="content-central">
                <div class="semiboxshadow text-center">
                    <img src="img/img-theme/shp.png" class="img-responsive" alt="">
                </div>
                <div class="content_info">
                    <div class="paddings-mini">
                        <div class="container padding-gallery-bottom">
                            <div class="portfolioFilter">
                                <a href="#" data-filter="*" class="current">Show All</a>
                                <a href="#beach" data-filter=".beach">Beach</a>
                                <a href="#nature" data-filter=".nature">Nature</a>
                                <a href="#extreme" data-filter=".extreme">Extreme</a>
                                <a href="#romantic" data-filter=".romantic">Romantic</a>
                            </div>
                            <div class="portfolioContainer">
                                <div class="col-xs-12 col-sm-6 col-md-3 nature">
                                    <div class="img-hover">
                                        <img src="img/gallery-2/1.jpg" alt="" class="img-responsive">
                                        <div class="overlay"><a href="img/gallery-2/1.jpg" class="fancybox"><i class="fa fa-plus-circle"></i></a></div>
                                    </div>
                                    <div class="info-gallery">
                                        <h3>
                                            The Large Everest Mount<br>
                                            <span>HIMALAYA MOUNTAINS</span>
                                        </h3>
                                        <!--<hr class="separator">
                                        <p>Donec sodales sagittis magna. Sed consequat, leo eget.</p>
                                        <ul class="starts">
                                            <li><a href="#"><i class="fa fa-star"></i></a></li>
                                            <li><a href="#"><i class="fa fa-star"></i></a></li>
                                            <li><a href="#"><i class="fa fa-star"></i></a></li>
                                            <li><a href="#"><i class="fa fa-star"></i></a></li>
                                            <li><a href="#"><i class="fa fa-star-half-empty"></i></a></li>
                                        </ul> -->
                                        <div class="content-btn"><a href="#" class="btn btn-primary">View Details</a></div>
                                        <!-- <div class="price"><span>$</span><b>From</b>45</div> -->
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-3 nature">
                                    <div class="img-hover">
                                        <img src="img/gallery-2/2.jpg" alt="" class="img-responsive">
                                        <div class="overlay"><a href="img/gallery-2/2.jpg" class="fancybox"><i class="fa fa-plus-circle"></i></a></div>
                                    </div>
                                    <div class="info-gallery">
                                        <h3>
                                            Chinese Mountains<br>
                                            <span>ORIENTAL NATURE</span>
                                        </h3>
                                        <!--<hr class="separator">
                                        <p>Donec sodales sagittis magna. Sed consequat, leo eget.</p>
                                        <ul class="starts">
                                            <li><a href="#"><i class="fa fa-star"></i></a></li>
                                            <li><a href="#"><i class="fa fa-star"></i></a></li>
                                            <li><a href="#"><i class="fa fa-star"></i></a></li>
                                            <li><a href="#"><i class="fa fa-star"></i></a></li>
                                            <li><a href="#"><i class="fa fa-star-half-empty"></i></a></li>
                                        </ul> -->
                                        <div class="content-btn"><a href="#" class="btn btn-primary">View Details</a></div>
                                        <!-- <div class="price"><span>$</span><b>From</b>25</div> -->
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-3 beach">
                                    <div class="img-hover">
                                        <img src="img/gallery-2/3.jpg" alt="" class="img-responsive">
                                        <div class="overlay"><a href="img/gallery-2/3.jpg" class="fancybox"><i class="fa fa-plus-circle"></i></a></div>
                                    </div>
                                    <div class="info-gallery">
                                        <h3>
                                            Polynesia Bora Bora<br>
                                            <span>FRENCH ISLANDS</span>
                                        </h3>
                                        <!--<hr class="separator">
                                        <p>Donec sodales sagittis magna. Sed consequat, leo eget.</p>
                                        <ul class="starts">
                                            <li><a href="#"><i class="fa fa-star"></i></a></li>
                                            <li><a href="#"><i class="fa fa-star"></i></a></li>
                                            <li><a href="#"><i class="fa fa-star"></i></a></li>
                                            <li><a href="#"><i class="fa fa-star"></i></a></li>
                                            <li><a href="#"><i class="fa fa-star-half-empty"></i></a></li>
                                        </ul> -->
                                        <div class="content-btn"><a href="#" class="btn btn-primary">View Details</a></div>
                                        <!-- <div class="price"><span>$</span><b>From</b>60</div> -->
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-3 beach">
                                    <div class="img-hover">
                                        <img src="img/gallery-2/4.jpg" alt="" class="img-responsive">
                                        <div class="overlay"><a href="img/gallery-2/4.jpg" class="fancybox"><i class="fa fa-plus-circle"></i></a></div>
                                    </div>
                                    <div class="info-gallery">
                                        <h3>
                                            Tayrona Park<br>
                                            <span>SANTA MARTA COLOMBIA</span>
                                        </h3>
                                        <!--<hr class="separator">
                                         <p>Donec sodales sagittis magna. Sed consequat, leo eget.</p>
                                        <ul class="starts">
                                            <li><a href="#"><i class="fa fa-star"></i></a></li>
                                            <li><a href="#"><i class="fa fa-star"></i></a></li>
                                            <li><a href="#"><i class="fa fa-star"></i></a></li>
                                            <li><a href="#"><i class="fa fa-star"></i></a></li>
                                            <li><a href="#"><i class="fa fa-star-half-empty"></i></a></li>
                                        </ul> -->
                                        <div class="content-btn"><a href="#" class="btn btn-primary">View Details</a></div>
                                        <!-- <div class="price"><span>$</span><b>From</b>75</div> -->
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-3 extreme">
                                    <div class="img-hover">
                                        <img src="img/gallery-2/5.jpg" alt="" class="img-responsive">
                                        <div class="overlay"><a href="img/gallery-2/5.jpg" class="fancybox"><i class="fa fa-plus-circle"></i></a></div>
                                    </div>
                                    <div class="info-gallery">
                                        <h3>
                                            Extreme Mountains<br>
                                            <span>CLIMB TO THE TOP</span>
                                        </h3>
                                        <!--<hr class="separator">
                                        <p>Donec sodales sagittis magna. Sed consequat, leo eget.</p>
                                        <ul class="starts">
                                            <li><a href="#"><i class="fa fa-star"></i></a></li>
                                            <li><a href="#"><i class="fa fa-star"></i></a></li>
                                            <li><a href="#"><i class="fa fa-star"></i></a></li>
                                            <li><a href="#"><i class="fa fa-star"></i></a></li>
                                            <li><a href="#"><i class="fa fa-star-half-empty"></i></a></li>
                                        </ul> -->
                                        <div class="content-btn"><a href="#" class="btn btn-primary">View Details</a></div>
                                        <!-- <div class="price"><span>$</span><b>From</b>20</div> -->
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-3 romantic">
                                    <div class="img-hover">
                                        <img src="img/gallery-2/6.jpg" alt="" class="img-responsive">
                                        <div class="overlay"><a href="img/gallery-2/6.jpg" class="fancybox"><i class="fa fa-plus-circle"></i></a></div>
                                    </div>
                                    <div class="info-gallery">
                                        <h3>
                                            Tower Eiffel<br>
                                            <span>ITALIE FRANCE</span>
                                        </h3>
                                        <!--<hr class="separator">
                                        <p>Donec sodales sagittis magna. Sed consequat, leo eget.</p>
                                        <ul class="starts">
                                            <li><a href="#"><i class="fa fa-star"></i></a></li>
                                            <li><a href="#"><i class="fa fa-star"></i></a></li>
                                            <li><a href="#"><i class="fa fa-star"></i></a></li>
                                            <li><a href="#"><i class="fa fa-star"></i></a></li>
                                            <li><a href="#"><i class="fa fa-star-half-empty"></i></a></li>
                                        </ul> -->
                                        <div class="content-btn"><a href="#" class="btn btn-primary">View Details</a></div>
                                        <!-- <div class="price"><span>$</span><b>From</b>15</div> -->
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-3 romantic">
                                    <div class="img-hover">
                                        <img src="img/gallery-2/7.jpg" alt="" class="img-responsive">
                                        <div class="overlay"><a href="img/gallery-2/7.jpg" class="fancybox"><i class="fa fa-plus-circle"></i></a></div>
                                    </div>
                                    <div class="info-gallery">
                                        <h3>
                                            Beautiful Venice<br>
                                            <span>LOCATED IN ITALY</span>
                                        </h3>
                                        <!--<hr class="separator">
                                        <p>Donec sodales sagittis magna. Sed consequat, leo eget.</p>
                                        <ul class="starts">
                                            <li><a href="#"><i class="fa fa-star"></i></a></li>
                                            <li><a href="#"><i class="fa fa-star"></i></a></li>
                                            <li><a href="#"><i class="fa fa-star"></i></a></li>
                                            <li><a href="#"><i class="fa fa-star"></i></a></li>
                                            <li><a href="#"><i class="fa fa-star-half-empty"></i></a></li>
                                        </ul> -->
                                        <div class="content-btn"><a href="#" class="btn btn-primary">View Details</a></div>
                                        <!-- <div class="price"><span>$</span><b>From</b>80</div> -->
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-3 romantic">
                                    <div class="img-hover">
                                        <img src="img/gallery-2/8.jpg" alt="" class="img-responsive">
                                        <div class="overlay"><a href="img/gallery-2/8.jpg" class="fancybox"><i class="fa fa-plus-circle"></i></a></div>
                                    </div>
                                    <div class="info-gallery">
                                        <h3>
                                            Romantic Night<br>
                                            <span>BEAUTIFUL BEACHES</span>
                                        </h3>
                                        <!--<hr class="separator">
                                         <p>Donec sodales sagittis magna. Sed consequat, leo eget.</p>
                                        <ul class="starts">
                                            <li><a href="#"><i class="fa fa-star"></i></a></li>
                                            <li><a href="#"><i class="fa fa-star"></i></a></li>
                                            <li><a href="#"><i class="fa fa-star"></i></a></li>
                                            <li><a href="#"><i class="fa fa-star"></i></a></li>
                                            <li><a href="#"><i class="fa fa-star-half-empty"></i></a></li>
                                        </ul> -->
                                        <div class="content-btn"><a href="#" class="btn btn-primary">View Details</a></div>
                                        <!-- <div class="price"><span>$</span><b>From</b>58</div> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <?php include_once './footer.php'; ?>
        </div>
        <script src="js/jquery.js"></script>
        <script src="js/jquery-ui.1.10.4.min.js"></script>
        <script src="js/nav/jquery.sticky.js" type="text/javascript"></script>
        <script type="text/javascript" src="js/theme-options/jquery.cookies.js"></script>
        <script type="text/javascript" src="js/bootstrap/bootstrap.js"></script>
        <script type="text/javascript" src="js/totop/jquery.ui.totop.js"></script>
        <!-- <script type="text/javascript" src="js/rs-plugin/js/jquery.themepunch.tools.min.js"></script> -->
        <!-- <script type='text/javascript' src='js/rs-plugin/js/jquery.themepunch.revolution.min.js'></script> -->
        <script type="text/javascript" src="js/fancybox/jquery.fancybox.js"></script>
        <script src="js/carousel/carousel.js"></script>
        <script src="js/filters/jquery.isotope.js" type="text/javascript"></script>
        <!-- <script type="text/javascript" src="js/bootstrap/bootstrap-slider.js"></script> -->
        <!-- <script type="text/javascript" src="js/main.js"></script> -->
        <script type="text/javascript">
        $(document).ready(function($) {
                $(window).load(function() {
                var $container = $('.portfolioContainer');
                $container.isotope({
                    filter: '*',
                    animationOptions: {
                        duration: 750,
                        easing: 'linear',
                        queue: false
                    }
                });
                $('.portfolioFilter a').click(function() {
                    $('.portfolioFilter .current').removeClass('current');
                    $(this).addClass('current');
                    var selector = $(this).attr('data-filter');
                    $container.isotope({
                        filter: selector,
                        animationOptions: {
                            duration: 750,
                            easing: 'linear',
                            queue: false
                        }
                    });
                    return false;
                });
            });
        });
    </script>
    </body>
</html>