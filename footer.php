<footer id="footer" class="footer">
    <div class="container">
        <div class="row">

            <!--            <div class="col-md-5">
                            <div class="title-footer">
                                <h2>Save on your plans!
                                    <br> <span>Select Travelia Theme And Receive</span>
                                    <br> our discounts by e-mail.</h2>
                            </div>
                            <p>You can choose your favorite destination and start planning your long-awaited vacation. We offer thousands of destinations and have a wide variety of hotels so that you can host and enjoy your stay without problems. Book now your trip travelia.com.</p>
                        </div>-->

            <div class="col-md-12">
                <div class="row">

                    <div class="col-md-2">
                        <h3>FOLLOW US</h3>
                        <ul class="social">
                            <li class="facebook"><span><i class="fa fa-facebook"></i></span><a href="https://www.facebook.com/vrholidays555" target="_blank">Facebook</a></li>
                            <li class="twitter"><span><i class="fa fa-twitter"></i></span><a href="https://twitter.com/vrholidays" target="_blank">Twitter</a></li>
                            <li class="instagram"><span><i class="fa fa-instagram"></i></span><a href="https://www.instagram.com/vrholidays555/" target="_blank">Instagram</a></li>
                            <!--<li class="github"><span><i class="fa fa-github"></i></span><a href="#">Github</a></li>-->
                        </ul>
                    </div>


                    <div class="col-md-3">
                        <h3>TRAVEL SPECIALISTS </h3>
                        <ul>
                            <li><i class="fa fa-check"></i> <a href="#">International Tours </a></li>
                            <li><i class="fa fa-check"></i> <a href="#">Domestic Tours</a></li>
                            <li><i class="fa fa-check"></i> <a href="#">Flight Ticketing</a></li>
                            <li><i class="fa fa-check"></i> <a href="#">Travel Insurance</a></li>
                        </ul>
                    </div>


                    <div class="col-md-3">
                        <h3>CONTACT US</h3>
                        <ul class="contact_footer">
                            <li><i class="fa fa-envelope"></i><a href="mailto:vrholidays@outlook.com">vrholidays@outlook.com

<!--                                    <span class="__cf_email__" data-cfemail="72170a131f021e1732170a131f021e175c111d1f">[email&#160;protected]</span>
                                    
                                    <script data-cfhash='f9e31' type="text/javascript">/* <![CDATA[ */!function (t, e, r, n, c, a, p) {
                                        try {
                                            t = document.currentScript || function () {
                                                for (t = document.getElementsByTagName('script'), e = t.length; e--; )
                                                    if (t[e].getAttribute('data-cfhash'))
                                                        return t[e]
                                            }();
                                            if (t && (c = t.previousSibling)) {
                                                p = t.parentNode;
                                                if (a = c.getAttribute('data-cfemail')) {
                                                    for (e = '', r = '0x' + a.substr(0, 2) | 0, n = 2; a.length - n; n += 2)
                                                        e += '%' + ('0' + ('0x' + a.substr(n, 2) ^ r).toString(16)).slice(-2);
                                                    p.replaceChild(document.createTextNode(decodeURIComponent(e)), c)
                                                }
                                                p.removeChild(t)
                                            }
                                        } catch (u) {
                                        }
                                    }()/* ]]> */</script>-->
                                </a>
                            </li>
                            <li>
                                <i class="fa fa-headphones"></i> <a href="#">O-  0268- 2555878
                                </a>
                            </li>
                            <li>
                                <i class="fa fa-headphones"></i> <a href="#">M- 7069430555
                                </a>
                            </li>
                            <li>
                                <i class="fa fa-headphones"></i> <a href="#">M- 7069467555
                                </a>
                            </li>
                            
                           
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <h3>OUR OFFICES</h3>
                        <ul class="contact_footer">
                            <li class="location">
                            <i class="fa fa-home"></i> <a href="https://www.google.com/maps/place/VR+HOLIDAYS/@22.698394,72.851142,15z/data=!4m5!3m4!1s0x0:0x89df3cd11e9e2669!8m2!3d22.698394!4d72.851142" target="_blank">Nadiad, Gujarat, India</a>
                                </li>
                                <li class="location">
                            <i class="fa fa-home"></i> <a href="#">Ahmedabad,Gujarat,India</a>
                                </li>
                                </ul>
                               
                        <!-- <form id="newsletterForm" action="#">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-envelope"></i>
                                        </span>
                                        <input class="form-control" placeholder="Your Name" name="name" type="text" required="required">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-envelope"></i>
                                        </span>
                                        <input class="form-control" placeholder="Your  Email" name="email" type="email" required="required">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <span class="input-group-btn">
                                        <button class="btn btn-primary" type="submit" name="subscribe">SIGN UP</button>
                                    </span>
                                </div>
                            </div>
                        </form>
                        <div id="result-newsletter"></div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="footer-down">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <p>&copy;VR Holidays . All Rights Reserved. 2019 - 2020</p>
                </div>
                <div class="col-md-7">

                    <ul class="nav-footer">
                        <li><a href="index.php">HOME</a></li>
                        <li><a href="aboutus.php">ABOUT US</a></li>
                        <li><a href="services.php">SERVICES</a></li>
                        <li><a href="gallery.php">GALLERY</a></li>
                        <li><a href="contact.php">CONTACT</a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

</footer>
