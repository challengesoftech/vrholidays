<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>VR Holidays Travel Agency, - Hotel Online Booking</title>
        <meta name="keywords" content=""/>
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/bootstrap/bootstrap.css" rel="stylesheet" media="screen" async>
        <!-- <link href="css/bootstrap/bootstrap-theme.css" rel="stylesheet" media="screen" async> -->
        <!-- <link href="css/bootstrap/bootstrap-slider.css" rel="stylesheet" media="screen" async> -->
        <link href="css/nav/style.css" rel="stylesheet" media="screen" async>
        <!-- <link href="js/fancybox/jquery.fancybox.css" rel="stylesheet" media="screen" async> -->
        <link href="css/skins/theme-options.css" rel="stylesheet" media="screen" async>
        <!-- <link href="css/carousel/owl.carousel.css" rel="stylesheet" media="screen" async> -->
        <!-- <link href="css/carousel/owl.theme.css" rel="stylesheet" media="screen" async> -->
        <link href="css/icons/font-awesome.css" rel="stylesheet" media="screen" async>
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,400,300,700" rel="stylesheet" media="screen" async>
        <link href="http://fonts.googleapis.com/css?family=Raleway" rel="stylesheet" media="screen" async>
        <link href="css/style.css" rel="stylesheet" media="screen" async>
        <link href="css/skins/green/green.css" rel="stylesheet" media="screen" async>
        <link href="css/theme-responsive.css" rel="stylesheet" media="screen" async>
        <link rel="shortcut icon" href="img/favicon-icon.png">
        <!-- <link rel="apple-touch-icon" href="img/icons/apple-touch-icon.png">
        <link rel="apple-touch-icon" sizes="72x72" href="img/icons/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="114x114" href="img/icons/apple-touch-icon-114x114.png"> -->

        <script src="js/modernizr.js"></script>
        <!--[if IE]>
                    <link rel="stylesheet" href="css/ie/ie.css">
                <![endif]-->
        <!--[if lte IE 8]>
                    <script src="js/responsive/html5shiv.js"></script>
                    <script src="js/responsive/respond.js"></script>
                <![endif]-->
    </head>
    <body>
        <div id="layout">
            <?php include_once './header.php'; ?>
            <div class="section-title-01">
            <div class="bg_parallax image_06_parallax"></div>
            <div class="opacy_bg_02">
                    <div class="container">
                        <h1>Contact</h1>
                        <div class="crumbs">
                            <ul>
                                <li><a href="index.php">Home</a></li>
                                <li>/</li>
                                <li>Contact Us</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <section class="content-central">
                <div class="semiboxshadow text-center">
                    <img src="img/img-theme/shp.png" class="img-responsive" alt="">
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="map">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14723.210106851686!2d72.851142!3d22.698394!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x89df3cd11e9e2669!2sVR%20HOLIDAYS!5e0!3m2!1sen!2sin!4v1594109176464!5m2!1sen!2sin" allowfullscreen="true" aria-hidden="false" tabindex="0"></iframe>
                            </div>
                        </div>
                   </div>
                </div>
        
                <div class="content_info">
                    <div class="paddings-mini">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-4">
                                    <aside>
                                        <h4>The Office</h4>
                                        <address>
                                            <strong>VR Holidays</strong><br>
                                            <i class="fa fa-map-marker"></i><strong>Address: </strong>3, Pitru Mandir, Mission Road, Opp. Shahid Smarak, Near Kheda Jilla Panchayat<br>
                                            <i class="fa fa-plane"></i><strong>City: </strong>Nadiad, Gujarat, India, 387002<br>
                                            <i class="fa fa-phone"></i> <abbr title="Phone">O:</abbr> 0268- 2555878<br/>
                                            <i class="fa fa-phone"></i> <abbr title="Phone">M:</abbr> 7069430555<br/>
                                            <i class="fa fa-phone"></i> <abbr title="Phone">M:</abbr> 7069467555
                                        </address>
                                        <address>
                                            <!--<strong>VR Holidays Emails</strong><br>-->
                                            <i class="fa fa-envelope"></i><strong>Email:</strong><a href="mailto:vrholidays@outlook.com"> vrholidays@outlook.com</a><br>
                                            <!--<i class="fa fa-envelope"></i><strong>Email:</strong><a href="http://html.iwthemes.com/cdn-cgi/l/email-protection#1330"> <span class="__cf_email__" data-cfemail="a2d1d7d2d2cdd0d6e2cbd5d6cac7cfc7d18cc1cdcf">[email&#160;protected]</span><script data-cfhash='f9e31' type="text/javascript">/* <![CDATA[ */!function(t, e, r, n, c, a, p){try{t = document.currentScript || function(){for (t = document.getElementsByTagName('script'), e = t.length; e--; )if (t[e].getAttribute('data-cfhash'))return t[e]}(); if (t && (c = t.previousSibling)){p = t.parentNode; if (a = c.getAttribute('data-cfemail')){for (e = '', r = '0x' + a.substr(0, 2) | 0, n = 2; a.length - n; n += 2)e += '%' + ('0' + ('0x' + a.substr(n, 2) ^ r).toString(16)).slice( - 2); p.replaceChild(document.createTextNode(decodeURIComponent(e)), c)}p.removeChild(t)}} catch (u){}}()/* ]]> */</script></a>-->
                                        </address>
                                    </aside>
                                    <hr class="tall">
                                </div>
                                <div class="col-md-8">
                                    <h3>Contact Form</h3>
                                    <p class="lead">
                                        You can choose your favorite destination and start planning your long-awaited vacation. We offer thousands of destinations and have a wide variety of hotels so that you can host and enjoy your stay without problems. Book now your trip with VR Holidays.

                                    </p>
                                    <form id="form-contact" class="form-theme" action="http://html.iwthemes.com/travelia/run/php/send-mail.php">
                                        <input type="text" placeholder="Name" name="Name" required="">
                                        <input type="email" placeholder="Email" name="Email" required="">
                                        <input type="number" placeholder="Phone" name="Phone" required="">
                                        <textarea placeholder="Your Message" name="message" required=""></textarea>
                                        <input type="submit" name="Submit" value="Send Message" class="btn btn-primary">
                                    </form>
                                    <div id="result"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
             </section>
            <?php include_once './footer.php'; ?>
        </div>
        <script src="js/jquery.js"></script>
        <script src="js/jquery-ui.1.10.4.min.js"></script>
        <script type="text/javascript" src="js/theme-options/jquery.cookies.js"></script>
        <script type="text/javascript" src="js/bootstrap/bootstrap.js"></script>
        <script src="js/nav/jquery.sticky.js" type="text/javascript"></script>
        <!-- <script type="text/javascript" src="js/totop/jquery.ui.totop.js"></script> -->
        <!-- <script type="text/javascript" src="js/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
        <script type='text/javascript' src='js/rs-plugin/js/jquery.themepunch.revolution.min.js'></script> -->
        <!-- <script type="text/javascript" src="js/fancybox/jquery.fancybox.js"></script>
        <script src="js/carousel/carousel.js"></script>
        <script src="js/filters/jquery.isotope.js" type="text/javascript"></script>
        <script type="text/javascript" src="js/bootstrap/bootstrap-slider.js"></script> -->
        <!-- <script type="text/javascript" src="js/main.js"></script> -->
       </body>
</html>
