<!DOCTYPE html>
<html lang="en">
    <head>

        <meta charset="utf-8">
        <title>VR Holidays Travel Agency, - Hotel Online Booking</title>
        <meta name="keywords" content=""/>
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/bootstrap/bootstrap.css" rel="stylesheet" media="screen" async>
        <link href="css/bootstrap/bootstrap-theme.css" rel="stylesheet" media="screen" async>
        <!-- <link href="css/bootstrap/bootstrap-slider.css" rel="stylesheet" media="screen" async> -->
        <link href="css/nav/style.css" rel="stylesheet" media="screen" async>
        <link href="js/fancybox/jquery.fancybox.css" rel="stylesheet" media="screen" async>
        <link href="css/skins/theme-options.css" rel="stylesheet" media="screen" async>
        <link href="css/carousel/owl.carousel.css" rel="stylesheet" media="screen" async>
        <link href="css/carousel/owl.theme.css" rel="stylesheet" media="screen" async>
        <link href="css/icons/font-awesome.css" rel="stylesheet" media="screen" async>
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,400,300,700" rel="stylesheet" media="screen" async>
        <link href="http://fonts.googleapis.com/css?family=Raleway" rel="stylesheet" media="screen" async>
        <link href="css/style.css" rel="stylesheet" media="screen" async>
        <link href="css/skins/green/green.css" rel="stylesheet" media="screen" async>
        <link href="css/theme-responsive.css" rel="stylesheet" media="screen" async>
        <link rel="shortcut icon" href="img/favicon-icon.png">
        <!-- <link rel="apple-touch-icon" href="img/icons/apple-touch-icon.png">
        <link rel="apple-touch-icon" sizes="72x72" href="img/icons/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="114x114" href="img/icons/apple-touch-icon-114x114.png"> -->
        <script src="js/modernizr.js"></script>
        <!--[if IE]>
                    <link rel="stylesheet" href="css/ie/ie.css">
                <![endif]-->
        <!--[if lte IE 8]>
                    <script src="js/responsive/html5shiv.js"></script>
                    <script src="js/responsive/respond.js"></script>
                <![endif]-->
    </head>
    <body>
        <div id="layout">
            <?php include_once './header.php'; ?>
            <div class="section-title-01">
                <div class="bg_parallax image_01_parallax"></div>
                <div class="opacy_bg_02">
                    <div class="container">
                        <h1>About Us</h1>
                        <div class="crumbs">
                            <ul>
                                <li><a href="index.php">Home</a></li>
                                <li>/</li>
                                <li>About Us</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <section class="content-central">
                <div class="semiboxshadow text-center">
                    <img src="img/img-theme/shp.png" class="img-responsive" alt="">
                </div>
                <div class="content_info">
                    <div class="skin_base">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="services-lines-info padding-sides color-white">
                                    <h2>TEAM MEMBERS</h2>
                                    <p class="lead">
                                    Expert in domestic and international packages.
                                        <span class="line"></span>
                                    </p>
                                    <p>Book Flight, Hotels and Holiday packages, free cancellation when the hotel so provides, 
                                    compare prices and find all the options for your vacation.</p>
                                    <!-- <a href="#" class="btn btn-primary">View Details</a> -->
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="row">
                                    <div id="team-carousel-02">

                                        
                                        <div class="item-team-01">
                                            <img src="img/team/2.jpg" alt="">
                                            <div class="info-team">
                                                <h4>Rinkesh Patel                                                    
                                                </h4>
                                                <p>We are team of experience productive and efficient <br>
                                                people in our VR Holidays to help with whatever you need.</p>
                                                <ul class="social-team">
                                                    <li><a href="https://www.facebook.com/rinkesh.patel.31" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                                    <li><a href="https://www.instagram.com/rinkeshpatel3/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                                                    <li><a href="https://www.linkedin.com/in/rinkeshpatel555/" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="item-team-01">
                                            <img src="img/team/4.jpg" alt="">
                                            <div class="info-team">
                                                <h4>Vyomesh Patel                                                    
                                                </h4>
                                                <p>We are team of experience productive and efficient <br>
                                                people in our VR Holidays to help with whatever you need.</p>
                                                <ul class="social-team">
                                                    <li><a href="https://www.facebook.com/red.vyomesh" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                                    <li><a href="https://www.instagram.com/vyom555patel/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                                                    <li><a href="http://linkedin.com/in/vyomesh-patel-111492153" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="content_info">
                    <div class="paddings">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6">
                                    <h3>Some words about us</h3>
                                  <!--  <p>You can choose your favorite destination and start planning your long-awaited vacation. We offer thousands of destinations and have a wide variety of hotels so that you can host and enjoy your stay without problems. Book now your trip vrholidays.com.</p> -->
                                    <p> Greetings from VR HOLIDAYS!!! 
                                        We feel obliged to introduce out ambition venture “VR Holidays” as entity, 
                                        which takes car of any tours and travel requirement of its esteemed clients at global level. 
                                        Since inception in 2016, VR Holidays has been venturing in to new geographic arena and making utmost effort to make best deal available to its guests.
                                        Apart from Ticketing (both Domestic and International). 
                                        We excel into designing Tailor made package for domestic, international for inbound and outbound tourists.  
                                        Worldwide Hotel Booking, Visa and passport, Travel Insurance, Local and international Transfers are also included in our services.
                                        VR Holidays is a one stop enterprise that offers the complete range of travel related services.
                                        Superior knowledge, efficient planning and ability to anticipate and resolve potential problems along the way are reasons behind our success.
                                        We Make tour travel FUN, SAFE,ECONOMICAL, INFORMATIVE, COMFORTABLE & MEMORABLE. </p>
                                   
                                    <div class="row">
                                      <!--  <div class="col-md-6">
                                            <ul class="list-styles">
                                                <li><i class="fa fa-check"></i> <a href="#">World Travel</a></li>
                                                <li><i class="fa fa-check"></i> <a href="#">First Class Flights</a></li>
                                                <li><i class="fa fa-check"></i> <a href="#">5 Star Accommodations</a></li>
                                                <li><i class="fa fa-check"></i> <a href="#">Inclusive Packages</a></li>
                                            </ul>
                                        </div>
                                        <div class="col-md-6">
                                            <ul class="list-styles">
                                                <li><i class="fa fa-check"></i> <a href="#">Latest Model Vehicles</a></li>
                                                <li><i class="fa fa-check"></i> <a href="#">Best Price Guarantee</a></li>
                                                <li><i class="fa fa-check"></i> <a href="#">Great Experience</a></li>
                                                <li><i class="fa fa-check"></i> <a href="#">Great Travel Theme</a></li>
                                            </ul>
                                        </div> -->
                                    </div>
                                </div>
                                <div class="col-md-6">

                                    <div id="single-carousel">
                                        <div class="img-hover">
                                            <div class="overlay"> <a href="img/gallery-2/1.jpg" class="fancybox"><i class="fa fa-plus-circle"></i></a></div>
                                            <img src="img/gallery-2/1.jpg" alt="" class="img-responsive">
                                        </div>
                                        <div class="img-hover">
                                            <div class="overlay"> <a href="img/gallery-2/2.jpg" class="fancybox"><i class="fa fa-plus-circle"></i></a></div>
                                            <img src="img/gallery-2/2.jpg" alt="" class="img-responsive">
                                        </div>
                                        <div class="img-hover">
                                            <div class="overlay"> <a href="img/gallery-2/3.jpg" class="fancybox"><i class="fa fa-plus-circle"></i></a></div>
                                            <img src="img/gallery-2/3.jpg" alt="" class="img-responsive">
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="content_info overflow-hidde">
                    <video class="bg_video" preload="auto" autoplay="autoplay" playsinline="" loop="" muted="" poster="img/slide/4.jpg" data-setup="{}">
                        <source src="img/video/video.mp4" type="video/mp4">
                        <source src="img/video/video.ogg" type="video/ogg">
                        <source src="img/video/video.webm" type="video/webm">
                    </video>
                    <div class="opacy_bg_02 padding-bottom">
                        <div class="container wow fadeInUp">
                            <div class="row text-center">
                                <div class="col-md-12">

                                    <div class="container">
                                        <div class="row">
                                            <div class="titles">
                                                <h2><span>Why</span>You <span>Book</span> in <span> VR holidays</span><span>?</span></h2>
                                                <i class="fa fa-plane"></i>
                                                <hr class="tall">
                                            </div>
                                        </div>
                                    </div>

                                    <p>Find a wide variety of airline tickets and flights, hotels, tour packages, 
                                        car rentals, cruises and more in VR Holidays.
                                        You can choose your favorite destination and start planning your long-awaited vacation. 
                                        You can also check availability of flights and hotels quickly and easily, 
                                        in order to find the option that best suits your needs.Book hotels and make payment facilities, 
                                        free cancellation when the hotel so provides,
                                         compare prices and find all the options for your vacation.</p>
                                    <!-- <div class="row text-center padding-top-mini"> 
                                        <a href="#" class="btn btn-primary">View Details</a>
                                        <a href="#" class="btn btn-primary">Purchase Theme</a>
                                    </div>-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
              <!--  <div class="section-twitter">
                    <i class="fa fa-twitter icon-big"></i>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="text-center">
                                    <h2>Our recents <span class="text-resalt">twitts</span>.</h2>
                                </div>
                                <div id="twitter"></div>
                            </div>
                        </div>
                    </div>
                </div><-->
                <div class="content_info">
                    <div class="content_resalt border-top">
                        <div class="container">
                            <div class="row paddings">
                                <div class="col-md-7 col-sm-8">
                                    <h2>We Provide You An Ultimate Travel Experience</h2>
                                    <p class="lead">
                                    Save on your plans! Select VR Holidays and Receive our discounts by e-mail.
                                        <span class="line"></span>
                                    </p>
                                   

                                    <div class="row">

                                        <div class="col-md-12">
                                            <div class="item-number-info">
                                                <div class="large-number">
                                                    1.
                                                </div>
                                                <div class="info-large-number">
                                                    <h5>Great discount</h5>
                                                    <p>For over 4 years offering the best plans at the best price with exclusive discounts.</p>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-md-12">
                                            <div class="item-number-info">
                                                <div class="large-number">
                                                    2.
                                                </div>
                                                <div class="info-large-number">
                                                    <h5>Better Services Experience</h5>
                                                    <p>Experience Great service by VR holidays while
                                                         you are on vacation and make best memorable Vacation with your family and friends.</p>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-md-12">
                                            <div class="item-number-info">
                                                <div class="large-number">
                                                    3.
                                                </div>
                                                <div class="info-large-number">
                                                    <h5>pays the Best price </h5>
                                                    <p>All days Escape at the best price in offers for our products.</p>
                                                </div>
                                            </div>
                                        </div>
                                     

                                    </div>

                                    <!-- <a href="#" class="btn btn-primary">View Details</a> -->
                                </div>
                            </div> 
                        </div>
                        <div class="image-container col-md-5 col-sm-3 pull-right">
                            <div class="bg_parallax image_01_parallax"></div>
                        </div>
                    </div>
                </div>
            </section>
            <?php include_once './footer.php'; ?>
        </div>
        <script src="js/jquery.js"></script>
        <script src="js/jquery-ui.1.10.4.min.js"></script>
        <script src="js/nav/jquery.sticky.js" type="text/javascript"></script>
        <script type="text/javascript" src="js/theme-options/jquery.cookies.js"></script>
        <script type="text/javascript" src="js/bootstrap/bootstrap.js"></script>
        <!-- <script type="text/javascript" src="js/totop/jquery.ui.totop.js"></script> -->
        <!-- <script type="text/javascript" src="js/rs-plugin/js/jquery.themepunch.tools.min.js"></script> -->
        <!-- <script type='text/javascript' src='js/rs-plugin/js/jquery.themepunch.revolution.min.js'></script> -->
        <script type="text/javascript" src="js/fancybox/jquery.fancybox.js"></script>
        <script src="js/carousel/carousel.js"></script>
        <!-- <script src="js/filters/jquery.isotope.js" type="text/javascript"></script> -->
        <script type="text/javascript" src="js/bootstrap/bootstrap-slider.js"></script>
        <!-- <script type="text/javascript" src="js/main.js"></script> -->
        <script type="text/javascript">
        $(document).ready(function($) {
            $("#team-carousel-02, #carousel-boxes-2").owlCarousel({
            autoPlay: 3200,
            items: 2,
            navigation: false,
            itemsDesktopSmall: [1024, 3],
            itemsTablet: [768, 2],
            itemsMobile: [500, 1],
            pagination: false
        });
        $("#single-carousel, #single-carousel-sidebar").owlCarousel({
            items: 1,
            autoPlay: 4000,
            navigation: true,
            autoHeight: true,
            slideSpeed: 400,
            singleItem: true,
            pagination: false
        });
        $(".fancybox").fancybox({
            openEffect: 'elastic',
            closeEffect: 'elastic',
            helpers: {
                title: {
                    type: 'inside'
                }
            }
     });
    });
    </script>
    </body>
</html>
