<header id="header" class="header">
    <nav class="flat-mega-menu">
        <label for="mobile-button"> <i class="fa fa-bars"></i></label> 
        <input id="mobile-button" type="checkbox">
        <ul class="collapse"> 
            <li class="title">
                <a href="index.php"><img src="img/logo.png" /></a>
                <div class="logo-shadow"></div>
                </li>
            <li><a href="index.php">HOME</a></li>
            <li><a href="aboutus.php">ABOUT US</a></li>
            <li><a href="services.php">SERVICES</a></li>
            <li><a href="gallery.php">GALLERY</a></li>
            <li><a href="contact.php">CONTACT</a></li>
            <!--                        <li> <a href="hotel-index.html">HOTELS</a>
                                        <ul class="drop-down one-column hover-fade"> 
                                            <li><a href="hotel-index.html">Home Hotels</a></li>
                                            <li><a href="hotel-list-view.html">List View</a></li>
                                            <li><a href="hotel-grid-view.html">Grid View</a></li>
                                            <li><a href="hotel-detailed.html">Detailed</a></li>
                                        </ul>
                                    </li>-->
            <!--                        <li> <a href="#">PACKAGES</a>
                                        <div class="drop-down full-width hover-fade"> 
                                            <ul> 
                                                <li>
                                                    <h2><span>Punta</span> Cana</h2>
                                                    <a href="packages-index.html"><img src="img/gallery-2/1.jpg" alt="image 1"> </a>
                                                    <p>Punta Cana is a cape located east of the Dominican Republic in the province of La Altagracia.</p>
                                                    <a href="packages-index.html" class="btn btn-primary">View Details</a>
                                                </li>
                                            </ul>
                                            <ul> 
                                                <li>
                                                    <h2><span>Santa</span> Marta</h2>
                                                    <a href="packages-index.html"><img src="img/gallery-2/2.jpg" alt="image 1"> </a>
                                                    <p>Santa Marta, officially Tourism, Cultural and Historic District of Santa Marta is a Colombian city.</p>
                                                    <a href="packages-index.html" class="btn btn-primary">View Details</a>
                                                </li>
                                            </ul>
                                            <ul> 
                                                <li>
                                                    <h2><span>Isla</span> de San Andres</h2>
                                                    <a href="packages-index.html"><img src="img/gallery-2/3.jpg" alt="image 1"> </a>
                                                    <p>San Andres Island is the largest of the islands forming part of the Archipelago of San Andrés.</p>
                                                    <a href="packages-index.html" class="btn btn-primary">View Details</a>
                                                </li>
                                            </ul>
                                            <ul> 
                                                <li>
                                                    <h2><span>Cartagena</span> de Indias</h2>
                                                    <a href="packages-index.html"><img src="img/gallery-2/4.jpg" alt="image 1"> </a>
                                                    <p>Cartagena de Indias, officially Tourist and Cultural District of Cartagena de Indias Cartagena de Indias abbreviated.</p>
                                                    <a href="packages-index.html" class="btn btn-primary">View Details</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>-->


            <li class="social-bar"> <a href="#">FOLLOW US</a>
                <ul class="drop-down hover-zoom">
                <li> <a href="https://www.facebook.com/vrholidays555" target="_blank"><i class="fa fa-facebook"></i> </a> </li>
                    <li> <a href="https://twitter.com/vrholidays" target="_blank"><i class="fa fa-twitter"></i> </a> </li>
                    <li> <a href="javascripit:void(0)" target="_blank"><i class="fa fa-instagram"></i> </a> </li>
                    
                   
                </ul>
            </li>
          <!--  <li class="login-form"> <i class="fa fa-user"></i> 
                <ul class="drop-down hover-expand">
                    <li>
                        <form method="post" action="#">
                            <table>
                                <tr>
                                    <td colspan="2">
                                        <input type="email" required="required" name="email" placeholder="Your email address">
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <input type="password" required="required" name="password" placeholder="Password">
                                    </td>
                                </tr>
                                <tr>
                                    <td> <input type="submit" value="Login"> </td>
                                    <td> <label> <input type="checkbox" name="check_box"> Keep me signed in </label> </td>
                                </tr>
                            </table>
                        </form>
                    </li>
                </ul>
            </li>
            <li class="search-bar"> <i class="fa fa-search"></i> 
                <ul class="drop-down hover-expand">
                    <li>
                        <form method="post" action="#">
                            <table>
                                <tr>
                                    <td> <input type="search" required="required" name="serach_bar" placeholder="Type Keyword Here"> </td>
                                    <td> <input type="submit" value="Search"> </td>
                                </tr>
                            </table>
                        </form>
                    </li>
                </ul>
            </li>-->
        </ul>
    </nav>
</header>
